package com.grd.cpay.core.console.vo;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class UserAuth extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userCode;
	private String nickName;

	public UserAuth(String userCode, String username, String password, Collection<? extends GrantedAuthority> authorities, String nickName) {
		super(username, password, authorities);
		this.userCode = userCode;
		this.nickName = nickName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

}
