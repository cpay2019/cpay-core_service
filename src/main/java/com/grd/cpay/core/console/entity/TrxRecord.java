package com.grd.cpay.core.console.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "TRX_RECORD")
public class TrxRecord {

    @Id
    @Column(name = "trx_code")
    private String trxCode;

    @ManyToOne(optional=true)
    @JoinColumn(name="tpp_code")
    private MerTppSetting merTppSetting;

    @Column(name = "mer_order_no")
    private String merOrderNo;

    @Column(name = "tpp_transaction_id")
    private String tppTransactionId;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "total_amount")
    private BigDecimal totalAmount;

    @Column(name = "trx_status")
    private String trxStatus;

    @Column(name = "trx_dt")
    private Date trxDt;

    @Column(name = "create_by")
    private String createBy;

    @Column(name = "create_dt")
    private Timestamp createDt;

    @Column(name = "modify_by")
    private String modifyBy;

    @Column(name = "modify_dt")
    private Timestamp modifyDt;

    public String getTrxCode() {
        return trxCode;
    }

    public void setTrxCode(String trxCode) {
        this.trxCode = trxCode;
    }

    public String getMerOrderNo() {
        return merOrderNo;
    }

    public void setMerOrderNo(String merOrderNo) {
        this.merOrderNo = merOrderNo;
    }

    public String getTppTransactionId() {
        return tppTransactionId;
    }

    public void setTppTransactionId(String tppTransactionId) {
        this.tppTransactionId = tppTransactionId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTrxStatus() {
        return trxStatus;
    }

    public void setTrxStatus(String trxStatus) {
        this.trxStatus = trxStatus;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Timestamp getCreateDt() {
        return createDt;
    }

    public void setCreateDt(Timestamp createDt) {
        this.createDt = createDt;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public Timestamp getModifyDt() {
        return modifyDt;
    }

    public void setModifyDt(Timestamp modifyDt) {
        this.modifyDt = modifyDt;
    }

    public MerTppSetting getMerTppSetting() {
        return merTppSetting;
    }

    public void setMerTppSetting(MerTppSetting merTppSetting) {
        this.merTppSetting = merTppSetting;
    }

    public Date getTrxDt() {
        return trxDt;
    }

    public void setTrxDt(Date trxDt) {
        this.trxDt = trxDt;
    }
}
