package com.grd.cpay.core.console.enums;

public enum LoginStatusEnum {
	lock ("此帳號已被鎖定"),
	invalid ("帳號密碼錯誤");

    private final String name;       

    private LoginStatusEnum(String s) {
        name = s;
    }

	public String getName() {
		return name;
	}

}
