package com.grd.cpay.core.console.security;

import com.grd.cpay.core.console.service.AdmUserService;
import com.grd.cpay.core.console.entity.AdmUser;
import com.grd.cpay.core.console.vo.UserAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class CpayUserDetailsService {
	
	@Autowired
	private AdmUserService admUserService;

	public UserAuth loadUser() {
		
		Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
		AdmUser admUser = admUserService.getAdmUserByUsername(currentUser.getName());
		return new UserAuth(admUser.getUserCode(), admUser.getUsername(), admUser.getPassword(), currentUser.getAuthorities(), admUser.getNickName());
	}

}
