package com.grd.cpay.core.console.service.impl;

import com.grd.cpay.core.console.entity.MerTppSetting;
import com.grd.cpay.base.enums.ChannelType;
import com.grd.cpay.core.console.repository.MerTppSettingRepository;
import com.grd.cpay.core.console.service.MerTppSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MerTppSettingServiceImpl implements MerTppSettingService {

    @Autowired
    private MerTppSettingRepository merTppSettingRepository;

    @Override
    public MerTppSetting save(MerTppSetting merTppSetting) {
        return merTppSettingRepository.save(merTppSetting);
    }

    @Override
    public MerTppSetting findById(String tppCode) {
        Optional<MerTppSetting> optional =  merTppSettingRepository.findById(tppCode);
        if(optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }

    @Override
    public Page<MerTppSetting> findByPage(int page, int pagesize) {
        Pageable pageable = PageRequest.of(page, pagesize, new Sort(Sort.Direction.ASC, "createDt"));
        Page<MerTppSetting> merTppSettingPage = merTppSettingRepository.findAll(pageable);
        return merTppSettingPage;
    }

    @Override
    public void deleteById(String tppcode) {
        merTppSettingRepository.deleteById(tppcode);
    }

    @Override
    public List<MerTppSetting> findByChannelType(ChannelType channelType) {
        return merTppSettingRepository.findByChannelType(channelType);
    }
}
