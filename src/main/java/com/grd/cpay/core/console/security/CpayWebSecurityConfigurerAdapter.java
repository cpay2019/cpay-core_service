package com.grd.cpay.core.console.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.sql.DataSource;
import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class CpayWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

	@Autowired
    private DataSource dataSource;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		
		http.csrf().disable().cors()
				.and()
					.authorizeRequests()
					.antMatchers("/v2/api-docs").permitAll()
				.and()
        			.authorizeRequests().antMatchers("/console/**").access("hasRole('ROLE_ADMIN')").anyRequest().authenticated()
        		.and()
        			.formLogin()
        			.loginPage("/")
        			.loginProcessingUrl("/login").permitAll()
        			.successHandler(cpayAuthenticationSuccessHandler())
        			.failureHandler(cpayAuthenticationFailHandler())
        		.and()
        			.logout()
        			.logoutUrl("/logout")
        			.logoutSuccessUrl("/")
            .and()
            .httpBasic();
		
    }

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery("select username, password, active from ADM_USER where username=?")
                .authoritiesByUsernameQuery("select au.username, ar.role from ADM_USER au join ADM_ROLE ar on au.role_code=ar.role_code where au.username=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder());
    }

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.debug(true);
	    web
	       .ignoring()
	       .antMatchers("/actuator/**" ,"/openapi/**","/payment/**","/loginErr/**", "/forgotpwd/**", "/bootstrap/**", "/dist/**", "/fastclick/**", "/font-awesome/**", "/Ionicons/**", "/jquery/**", "/plugins/**", "/datatables/**");

		web.ignoring().antMatchers("/configuration/ui", "/swagger-resources", "/configuration/security",
				"/swagger-ui.html", "/webjars/**", "/swagger-resources/configuration/ui", "/swagge‌​r-ui.html",
				"/swagger-resources/configuration/security","/resources/static/docs/");
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration config = new CorsConfiguration();
		config.addAllowedHeader("*");
		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.setMaxAge(3600L);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/v2/api-docs", config);
		return source;
	}


	@Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
    @Bean
    public CpayAuthenticationSuccessHandler cpayAuthenticationSuccessHandler() {
        return new CpayAuthenticationSuccessHandler();
    }
    
    @Bean
    public CpayAuthenticationFailHandler cpayAuthenticationFailHandler() {
        return new CpayAuthenticationFailHandler();
    }
    
}
