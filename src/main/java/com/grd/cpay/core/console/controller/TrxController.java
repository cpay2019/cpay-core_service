package com.grd.cpay.core.console.controller;

import com.grd.cpay.core.console.service.TrxRecordService;
import com.grd.cpay.core.console.entity.TrxRecord;
import com.grd.cpay.core.console.vo.TrxVo;
import com.grd.cpay.core.console.vo.ViewPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = "console/trx")
public class TrxController extends CPayAbstractController {

    @Autowired
    private TrxRecordService trxRecordService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String login(Model model) {

        model.addAttribute("nickName", getUserAuth().getNickName());
        model.addAttribute("mainMenu", "trxRecord");
        return "console/trx";
    }

    @RequestMapping(value="/view", method = RequestMethod.GET)
    public @ResponseBody
    ViewPage<TrxVo> getTrx(
            @RequestParam(value="draw") Integer draw,
            @RequestParam(value="length") Integer length,
            @RequestParam(value="start") Integer start) {

        Page<TrxRecord> trxRecordPage = trxRecordService.findByPage((start/length), length);
        List<TrxVo> trxVoList = new ArrayList<>();
        if(trxRecordPage.getContent()!=null && trxRecordPage.getContent().size()>0) {
            for(TrxRecord trxRecord:trxRecordPage.getContent()) {
                TrxVo trxVo = convert(trxRecord);
                trxVoList.add(trxVo);
            }
        }
        return converToViewPage(draw, trxVoList, trxRecordPage.getTotalElements());
    }

    private TrxVo convert(TrxRecord trxRecord) {
        TrxVo trxVo = new TrxVo();
        trxVo.setTrxCode(trxRecord.getTrxCode());
        trxVo.setTppName(trxRecord.getMerTppSetting().getTppName());
        trxVo.setChannelType(trxRecord.getMerTppSetting().getChannelType());
        trxVo.setMerOrderNo(trxRecord.getMerOrderNo());
        trxVo.setProductId(trxRecord.getProductId());
        trxVo.setTotalAmount(trxRecord.getTotalAmount());
        trxVo.setTppTransactionId(trxRecord.getTppTransactionId());
        trxVo.setTrxStatus(trxRecord.getTrxStatus());
        return trxVo;
    }
}
