package com.grd.cpay.core.console.controller;

import com.grd.cpay.core.console.enums.LoginStatusEnum;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String login(Model model) {
        return "console/login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(
            @RequestParam(value="username") String username,
            @RequestParam(value="password") String password,
            Model model) {
        return "success";
    }

    @RequestMapping(value="/loginErr/{errorCode}", method = RequestMethod.GET)
    public String loginErr(@PathVariable("errorCode") String errorCode, Model model) {
        model.addAttribute("msgError", LoginStatusEnum.valueOf(errorCode).getName());
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model) {
        return "success";
    }
}
