package com.grd.cpay.core.console.service;

import com.grd.cpay.core.console.entity.MerTppSetting;
import com.grd.cpay.base.enums.ChannelType;
import org.springframework.data.domain.Page;

import java.util.List;

public interface MerTppSettingService {

    MerTppSetting save(MerTppSetting merTppSetting);

    MerTppSetting findById(String tppCode);

    Page<MerTppSetting> findByPage(int page, int pagesize);

    void deleteById(String tppcode);

    List<MerTppSetting> findByChannelType(ChannelType channelType);
}
