package com.grd.cpay.core.console.security;

import com.grd.cpay.core.console.service.AdmUserService;
import com.grd.cpay.core.console.entity.AdmUser;
import com.grd.cpay.core.console.enums.LoginStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CpayAuthenticationFailHandler implements AuthenticationFailureHandler {

	@Autowired
	private AdmUserService admUserService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse res, AuthenticationException auth)
			throws IOException, ServletException {

		String username = (String) req.getParameter(usernamePasswordAuthenticationFilter().getUsernameParameter());
	    String password = (String) req.getParameter(usernamePasswordAuthenticationFilter().getPasswordParameter());
        
		AdmUser admUser = admUserService.getAdmUserByUsername(username);
		
		if(admUser!=null) {
			if(!admUser.isActive()) {
				res.sendRedirect("/loginErr/" + LoginStatusEnum.lock.toString());
				return;
			}
			if(!bCryptPasswordEncoder.matches(password, admUser.getPassword())) {
				admUserService.updateErrLoginCnt(username, false);
				res.sendRedirect("/loginErr/" + LoginStatusEnum.invalid.toString());
				return;
			}
		} else {
			res.sendRedirect("/loginErr/" + LoginStatusEnum.invalid.toString());
			return;
		}
	}
	
	@Bean
    public UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter() {
        return new UsernamePasswordAuthenticationFilter();
    }

}
