package com.grd.cpay.core.console.security;

import com.grd.cpay.core.console.service.AdmUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CpayAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	@Autowired
	private AdmUserService admUserService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
			throws IOException, ServletException {

		User authUser = (User) auth.getPrincipal();
		admUserService.updateErrLoginCnt(authUser.getUsername(), true);
        
        res.sendRedirect("/console/tpp");
	}

}
