package com.grd.cpay.core.console.controller;

import com.grd.cpay.core.console.entity.MerTppSetting;
import com.grd.cpay.core.console.service.MerTppSettingService;
import com.grd.cpay.core.console.vo.TppVo;
import com.grd.cpay.core.console.vo.ViewPage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping(value = "console/tpp")
public class TppController extends CPayAbstractController {

    @Autowired
    private MerTppSettingService merTppSettingService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String login(Model model) {

        model.addAttribute("nickName", getUserAuth().getNickName());
        model.addAttribute("mainMenu", "tppSetting");
        return "console/tpp";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addGift(TppVo form, Model model) {

        MerTppSetting merTppSetting = new MerTppSetting();
        if(StringUtils.isNotBlank(form.getTppCode())) {
            merTppSetting = merTppSettingService.findById(form.getTppCode());
            merTppSetting.setModifyBy(getUserAuth().getUserCode());
            merTppSetting.setModifyDt(new Timestamp(new Date().getTime()));
            model.addAttribute("msgSuccess", "修改成功");
        } else {
            merTppSetting.setTppCode(UUID.randomUUID().toString());
            merTppSetting.setCreateBy(getUserAuth().getUserCode());
            merTppSetting.setCreateDt(new Timestamp(new Date().getTime()));
            model.addAttribute("msgSuccess", "新增成功");
        }
        merTppSetting.setTppName(form.getTppName());
        merTppSetting.setChannelType(form.getChannelType());
        merTppSetting.setAppId(form.getAppId());
        merTppSetting.setPrivateKey(form.getPrivateKey());
        merTppSettingService.save(merTppSetting);

        model.addAttribute("nickName", getUserAuth().getNickName());
        model.addAttribute("mainMenu", "tppSetting");
        return "console/tpp";
    }

    @RequestMapping(value="/view", method = RequestMethod.GET)
    public @ResponseBody ViewPage<TppVo> getTpp(
            @RequestParam(value="draw") Integer draw,
            @RequestParam(value="length") Integer length,
            @RequestParam(value="start") Integer start) {

        Page<MerTppSetting> merTppSettingPage = merTppSettingService.findByPage((start/length), length);
        List<TppVo> tppVoList = new ArrayList<>();
        if(merTppSettingPage.getContent()!=null && merTppSettingPage.getContent().size()>0) {
            for(MerTppSetting merTppSetting:merTppSettingPage.getContent()) {
                TppVo tppVo = convert(merTppSetting);
                tppVoList.add(tppVo);
            }
        }
        return converToViewPage(draw, tppVoList, merTppSettingPage.getTotalElements());
    }

    @RequestMapping(value="/info", method = RequestMethod.GET)
    public @ResponseBody TppVo getTppInfo(
            @RequestParam(value="tppCode") String tppCode) {

        MerTppSetting merTppSetting = merTppSettingService.findById(tppCode);
        return convert(merTppSetting);
    }

    @RequestMapping(value="/del", method = RequestMethod.GET)
    public @ResponseBody String delTpp(
            @RequestParam(value="tppCode") String tppCode) {
        merTppSettingService.deleteById(tppCode);
        return "success";
    }

    private TppVo convert(MerTppSetting merTppSetting) {
        TppVo tppVo = new TppVo();
        tppVo.setTppCode(merTppSetting.getTppCode());
        tppVo.setTppName(merTppSetting.getTppName());
        tppVo.setChannelType(merTppSetting.getChannelType());
        tppVo.setAppId(merTppSetting.getAppId());
        tppVo.setPrivateKey(merTppSetting.getPrivateKey());
        return tppVo;
    }
}
