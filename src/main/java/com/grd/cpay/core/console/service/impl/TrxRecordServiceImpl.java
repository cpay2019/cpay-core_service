package com.grd.cpay.core.console.service.impl;

import com.grd.cpay.core.console.service.TrxRecordService;
import com.grd.cpay.core.console.entity.TrxRecord;
import com.grd.cpay.core.console.repository.TrxRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TrxRecordServiceImpl implements TrxRecordService {

    @Autowired
    private TrxRecordRepository trxRecordRepository;

    @Override
    public Page<TrxRecord> findByPage(int page, int pagesize) {
        Pageable pageable = PageRequest.of(page, pagesize, new Sort(Sort.Direction.DESC, "createDt"));
        Page<TrxRecord> trxRecordPage = trxRecordRepository.findAll(pageable);
        return trxRecordPage;
    }

    @Override
    public void save(TrxRecord trxRecord) {
        trxRecordRepository.save(trxRecord);
    }

    @Override
    public TrxRecord findById(String trxCode) {
        Optional<TrxRecord> optional = trxRecordRepository.findById(trxCode);
        if(optional.isPresent()) {
            return optional.get();
        } else {
            return null;
        }
    }
}
