package com.grd.cpay.core.console.repository;

import com.grd.cpay.core.console.entity.MerTppSetting;
import com.grd.cpay.base.enums.ChannelType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MerTppSettingRepository extends JpaRepository<MerTppSetting, String> {

    List<MerTppSetting> findByChannelType(ChannelType channelType);
}
