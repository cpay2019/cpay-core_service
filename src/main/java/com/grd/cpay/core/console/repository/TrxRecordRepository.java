package com.grd.cpay.core.console.repository;

import com.grd.cpay.core.console.entity.TrxRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrxRecordRepository extends JpaRepository<TrxRecord, String> {
}
