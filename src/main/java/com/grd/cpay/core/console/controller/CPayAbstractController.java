package com.grd.cpay.core.console.controller;

import com.grd.cpay.core.console.security.CpayUserDetailsService;
import com.grd.cpay.core.console.vo.UserAuth;
import com.grd.cpay.core.console.vo.ViewPage;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CPayAbstractController {

	@Autowired
	private CpayUserDetailsService vpayUserDetailsService;
	
	protected UserAuth getUserAuth() {
		return vpayUserDetailsService.loadUser();
	}

	protected <T> ViewPage<T> converToViewPage(Integer draw, List<T> tList, Long recordsTotal) {
		ViewPage<T> viewPage = new ViewPage<T>();
		viewPage.setData(tList);
		viewPage.setDraw(draw);
		viewPage.setRecordsTotal(recordsTotal);
		viewPage.setRecordsFiltered(recordsTotal);
		return viewPage;
	}
}
