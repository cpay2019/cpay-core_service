package com.grd.cpay.core.console.service.impl;

import com.grd.cpay.core.console.service.AdmUserService;
import com.grd.cpay.core.console.entity.AdmUser;
import com.grd.cpay.core.console.repository.AdmUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdmUserServiceImpl implements AdmUserService {


    @Autowired
    private AdmUserRepository admUserRepository;

    @Override
    public AdmUser getAdmUserByUsername(String username) {
        return admUserRepository.getAdmUserByUsername(username);
    }

    @Override
    public int updateErrLoginCnt(String username, boolean loginSuccess) {
        // TODO
        return 0;
    }
}
