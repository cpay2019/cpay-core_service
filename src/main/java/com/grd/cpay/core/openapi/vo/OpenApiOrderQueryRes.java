package com.grd.cpay.core.openapi.vo;

import java.math.BigDecimal;

public class OpenApiOrderQueryRes {

    private String trxCode;
    private String trxStatus;
    private BigDecimal amount;

    public String getTrxCode() {
        return trxCode;
    }

    public void setTrxCode(String trxCode) {
        this.trxCode = trxCode;
    }

    public String getTrxStatus() {
        return trxStatus;
    }

    public void setTrxStatus(String trxStatus) {
        this.trxStatus = trxStatus;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
