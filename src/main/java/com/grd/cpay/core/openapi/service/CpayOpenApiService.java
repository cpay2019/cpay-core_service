package com.grd.cpay.core.openapi.service;

import com.grd.cpay.core.openapi.vo.OpenApiOrderRes;
import com.grd.cpay.base.enums.ChannelType;
import com.grd.cpay.core.openapi.vo.OpenApiOrderQueryRes;

import java.math.BigDecimal;

public interface CpayOpenApiService {

    OpenApiOrderRes order(BigDecimal amount, String merOrderNo, String productId, ChannelType channelType);

    OpenApiOrderQueryRes orderQuery(String outTradeNo);
}
