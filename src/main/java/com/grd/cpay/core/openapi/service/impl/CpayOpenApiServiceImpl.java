package com.grd.cpay.core.openapi.service.impl;

import com.fasterxml.uuid.Generators;
import com.grd.cpay.core.console.entity.MerTppSetting;
import com.grd.cpay.core.console.service.MerTppSettingService;
import com.grd.cpay.core.console.service.TrxRecordService;
import com.grd.cpay.core.openapi.vo.OpenApiOrderRes;
import com.grd.cpay.base.enums.ChannelType;
import com.grd.cpay.core.console.entity.TrxRecord;
import com.grd.cpay.core.openapi.service.CpayOpenApiService;
import com.grd.cpay.core.openapi.vo.OpenApiOrderQueryRes;
import com.grd.cpay.core.paymentgw.service.PaymentGwService;
import com.grd.cpay.core.paymentgw.vo.req.OrderReq;
import com.grd.cpay.core.paymentgw.vo.req.QueryReq;
import com.grd.cpay.core.paymentgw.vo.res.OrderRes;
import com.grd.cpay.core.paymentgw.vo.res.QueryRes;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class CpayOpenApiServiceImpl implements CpayOpenApiService {

    @Autowired
    private PaymentGwService paymentGwService;

    @Autowired
    private MerTppSettingService merTppSettingService;

    @Autowired
    private TrxRecordService trxRecordService;

    @Override
    public OpenApiOrderRes order(BigDecimal amount, String merOrderNo, String productId, ChannelType channelType) {

        List<MerTppSetting> merTppSettingList = merTppSettingService.findByChannelType(channelType);
        OpenApiOrderRes openApiOrderRes = new OpenApiOrderRes();
        if(merTppSettingList.size()>0) {
            int num = RandomUtils.nextInt(0, merTppSettingList.size());
            UUID uuid = Generators.timeBasedGenerator().generate();

            OrderReq req = new OrderReq();
            req.setApiKey(merTppSettingList.get(num).getPrivateKey());
            req.setAppId(merTppSettingList.get(num).getAppId());
            req.setChannelType(channelType);
            req.setOutTradeNo(uuid.toString().replace("-",""));
            req.setProductId(productId);
            req.setTotalFee(amount);

            Date trxDt = Calendar.getInstance().getTime();
            if(channelType==ChannelType.UNION) {
                req.setMchId(merTppSettingList.get(num).getAppId());
                req.setTimeStart(trxDt);
            }

            TrxRecord trxRecord = new TrxRecord();
            trxRecord.setTrxCode(req.getOutTradeNo());
            trxRecord.setMerTppSetting(merTppSettingList.get(num));
            trxRecord.setMerOrderNo(merOrderNo);
            trxRecord.setProductId(productId);
            trxRecord.setTotalAmount(amount);
            trxRecord.setTrxStatus("WAITING");
            trxRecord.setTrxDt(trxDt);
            trxRecord.setCreateBy("SYS");
            trxRecord.setCreateDt(new Timestamp(new Date().getTime()));
            trxRecordService.save(trxRecord);

            OrderRes res = paymentGwService.order(req);
            openApiOrderRes.setOutTradeNo(res.getOutTradeNo());
            openApiOrderRes.setCodeUrl(res.getCodeUrl());

        }
        return openApiOrderRes;
    }

    @Override
    public OpenApiOrderQueryRes orderQuery(String outTradeNo) {

        TrxRecord trxRecord = trxRecordService.findById(outTradeNo);
        OpenApiOrderQueryRes openApiOrderQueryRes = new OpenApiOrderQueryRes();
        openApiOrderQueryRes.setTrxCode(trxRecord.getTrxCode());
        openApiOrderQueryRes.setAmount(trxRecord.getTotalAmount());

        if(trxRecord!=null && !"SUCCESS".equals(trxRecord.getTrxStatus())) {
            QueryReq queryReq = new QueryReq();
            queryReq.setApiKey(trxRecord.getMerTppSetting().getPrivateKey());
            queryReq.setAppId(trxRecord.getMerTppSetting().getAppId());
            queryReq.setChannelType(trxRecord.getMerTppSetting().getChannelType());
            queryReq.setOutTradeNo(trxRecord.getTrxCode());
            queryReq.setTimeStart(trxRecord.getTrxDt());

            if(trxRecord.getMerTppSetting().getChannelType()==ChannelType.UNION) {
                queryReq.setMchId(trxRecord.getMerTppSetting().getAppId());
            }

            QueryRes res = paymentGwService.orderquery(queryReq);
            if(res!=null && res.getTradeState().equals("TRADE_SUCCESS")) {
                trxRecord.setTppTransactionId(res.getTransactionId());
                trxRecord.setTrxStatus("SUCCESS");
                trxRecordService.save(trxRecord);
            } else {
                openApiOrderQueryRes.setTrxStatus(trxRecord.getTrxStatus());
            }

        } else {
            openApiOrderQueryRes.setTrxStatus(trxRecord.getTrxStatus());
        }
        return openApiOrderQueryRes;
    }

}
