package com.grd.cpay.core.openapi.controller;

import com.grd.cpay.core.openapi.service.CpayOpenApiService;
import com.grd.cpay.core.openapi.vo.OpenApiOrderQueryRes;
import com.grd.cpay.core.openapi.vo.OpenApiOrderRes;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.grd.cpay.base.enums.ChannelType;
import com.grd.cpay.base.vo.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/openapi")
@Api(tags = "OpenAPI related apis")
public class CpayOpenApiController {

    @Autowired
    private CpayOpenApiService vpayOpenApiService;

    @ApiOperation("下單")
    @PostMapping("/order")
    public ResponseObject<OpenApiOrderRes> order(
            @RequestParam("amount")BigDecimal amount,
            @RequestParam("merOrderNo")String merOrderNo,
            @RequestParam("productId")String productId,
            @RequestParam("channelType") ChannelType channelType) {

        OpenApiOrderRes openApiOrderRes = vpayOpenApiService.order(amount, merOrderNo, productId, channelType);
        return new ResponseObject<>(openApiOrderRes);
    }

    @ApiOperation("查詢")
    @PostMapping("/orderQuery")
    public ResponseObject<OpenApiOrderQueryRes> orderQuery(
            @RequestParam("outTradeNo")String outTradeNo) {

        OpenApiOrderQueryRes openApiOrderQueryRes = vpayOpenApiService.orderQuery(outTradeNo);
        return new ResponseObject<>(openApiOrderQueryRes);
    }
}
