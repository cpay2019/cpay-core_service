package com.grd.cpay.core.paymentgw.service;

import com.grd.cpay.core.paymentgw.vo.req.OrderReq;
import com.grd.cpay.core.paymentgw.vo.req.QueryReq;
import com.grd.cpay.core.paymentgw.vo.res.OrderRes;
import com.grd.cpay.core.paymentgw.vo.res.QueryRes;

public interface PaymentGwService {

    OrderRes order(OrderReq orderReq);

    QueryRes orderquery(QueryReq queryReq);
}
