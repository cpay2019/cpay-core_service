package com.grd.cpay.core.paymentgw.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.grd.cpay.base.utils.JsonUtils;
import com.grd.cpay.base.vo.ResponseObject;
import com.grd.cpay.core.paymentgw.vo.req.OrderReq;
import com.grd.cpay.core.paymentgw.vo.req.QueryReq;
import com.grd.cpay.core.paymentgw.vo.res.OrderRes;
import com.grd.cpay.core.paymentgw.vo.res.QueryRes;
import com.grd.cpay.base.service.BaseHttpService;
import com.grd.cpay.core.paymentgw.service.PaymentGwService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class PaymentGwServiceImpl extends BaseHttpService implements PaymentGwService {

    @Value("${payment.gw.url}")
    private String PAYMENT_GW_URL;

    private static final String URI_ORDER = "/order";
    private static final String URI_ORDER_QUERY = "/orderquery";

    @Override
    public OrderRes order(OrderReq orderReq) {
        String res = sendHttpRequest(PAYMENT_GW_URL+URI_ORDER, HttpMethod.POST, JsonUtils.fromObject(orderReq));
        ResponseObject<OrderRes> responseObject = JsonUtils.fromJson(res, new TypeReference<ResponseObject<OrderRes>>() {
        });
        return responseObject.getData();
    }

    @Override
    public QueryRes orderquery(QueryReq queryReq) {
        String res = sendHttpRequest(PAYMENT_GW_URL+URI_ORDER_QUERY, HttpMethod.POST, JsonUtils.fromObject(queryReq));
        ResponseObject<QueryRes> responseObject = JsonUtils.fromJson(res, new TypeReference<ResponseObject<QueryRes>>() {
        });
        return responseObject.getData();
    }
}
