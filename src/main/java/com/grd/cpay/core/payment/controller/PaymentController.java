package com.grd.cpay.core.payment.controller;

import com.grd.cpay.base.enums.ChannelType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@Controller
@RequestMapping(value = "payment")
public class PaymentController {

    @RequestMapping(value = "/demo", method = RequestMethod.GET)
    public String demo(Model model) {
        return "payment/demo";
    }

    @RequestMapping(value = "/prepay", method = RequestMethod.POST)
    public String prepay(Model model,
                 @RequestParam(value="amount") BigDecimal amount,
                 @RequestParam(value="merOrderNo") String merOrderNo,
                 @RequestParam(value="productId") String productId) {
        model.addAttribute("amount", amount);
        model.addAttribute("merOrderNo", merOrderNo);
        model.addAttribute("productId", productId);
        return "payment/prepay";
    }

    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    public String pay(Model model,
                         @RequestParam(value="amount") BigDecimal amount,
                         @RequestParam(value="merOrderNo") String merOrderNo,
                         @RequestParam(value="productId") String productId,
                         @RequestParam(value="channelType") ChannelType channelType) {
        model.addAttribute("amount", amount);
        model.addAttribute("merOrderNo", merOrderNo);
        model.addAttribute("productId", productId);
        model.addAttribute("channelType", channelType);
        model.addAttribute("channelTypeName", channelType.getDisplay());
        return "payment/pay";
    }
}
