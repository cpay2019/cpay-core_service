package com.grd.cpay.base.utils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

public class JaxbUtils {

    public static <T> String jaxbObjectToXmlString (T t, Class<T> clazz) {
        try {
            StringWriter sw = new StringWriter();
            JAXBContext context = JAXBContext.newInstance(clazz);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.marshal(t, sw);
            return sw.toString();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return new String();
    }

    public static <T> T jaxbXmlStringToObject(String s, Class<T> clazz) {
        try {
            JAXBContext context = JAXBContext.newInstance(clazz);
            Unmarshaller un = context.createUnmarshaller();
            T t = (T) un.unmarshal(new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8)));
            return t;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
