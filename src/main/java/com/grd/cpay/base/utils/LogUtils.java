package com.grd.cpay.base.utils;

import org.apache.logging.log4j.LogManager;

public class LogUtils {
	
    public static void debug(String tag, String message) {
    	LogManager.getLogger(tag).debug(message);
    }

    public static void info(String tag, String message) {
    	LogManager.getLogger(tag).info(message);
    }

    public static void warn(String tag, String message) {
    	LogManager.getLogger(tag).warn(message);
    }

    public static void error(String tag, String message) {
    	LogManager.getLogger(tag).error(message);
    }

    public static void error(String tag, Exception e) {
    	LogManager.getLogger(tag).error(e);
    }

}
