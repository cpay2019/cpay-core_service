package com.grd.cpay.base.service;

import com.grd.cpay.base.utils.JsonUtils;
import com.grd.cpay.base.utils.LogUtils;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

public class BaseHttpService {

    private static final String TAG = "BaseHttpService";

    /**
     *
     * @param url
     * @param httpMethod
     * @param bodyStr
     * @return
     */
    protected String sendHttpRequest(String url, HttpMethod httpMethod, String bodyStr) {

        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            RequestEntity requestEntity = new RequestEntity(bodyStr, headers, httpMethod, new URI(url));
            LogUtils.info(TAG, String.format("Request Url:%s, Method:%s, Body:%s", requestEntity.getUrl(), requestEntity.getMethod(), bodyStr));
            HttpEntity<String> response = restTemplate.exchange(requestEntity, String.class);
            LogUtils.info(TAG, "Response:"+ JsonUtils.fromObject(response.getBody()));
            return response.getBody();
        } catch (Exception e) {
            LogUtils.error(TAG, e);
            LogUtils.error(TAG, "url: " +url);
            return null;
        }

    }

}
