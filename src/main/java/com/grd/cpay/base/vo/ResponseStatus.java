package com.grd.cpay.base.vo;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public enum ResponseStatus {

	SUCCESS("success", "0","成功"),

	/** 平台錯誤 **/
	TOKEN_EXPIRED("token expired","27","無效的SessionKey參數"),
	PARAMETERS_ERROR("parameters error", "43","參數錯誤"),
	TOKEN_INVALID("invalid access token", "44","無效的access token"),
	ACCESS_DENIED("authority error, access denied", "55", "無訪問權限"),
	FAIL("general internal fail", "99", "非預期性錯誤"),

	/** User API 相關錯誤  **/
	USER_NOT_EXIST("can not find user", "501","User資料不存在"),
	SAVE_USER_FAIL("save user fail", "502","User 修改失敗"),
	USER_ACTIVE_FAIL("user active fail", "503","激活User失敗"),
	USER_AUDIT_FAIL("user audit fail", "504","审核User失敗"),
	//VERIFY_CODE_ERROR("verify code error", "504", "驗證碼錯誤"),
	//VERIFY_CODE_INVALID("verify code is invalid", "505","驗證碼過期"),
	USER_NAME_EXISTED("user name existed", "506","用戶名重覆"),
	//CONTACT_EXISTED("contact data existed", "507","聯繫方式重覆"),
	TOKEN_ERROR("token error","510","註冊User失敗"),
	SEARCH_PERIOD_ERROR("search period error", "511", "搜尋日期區間錯誤"),
	UPDATE_USER_STATUS_FAIL("update user status fail", "512", "修感User狀態錯誤"),

	/**ADMIN_USER 相關*/
	ADMIN_USER_NOT_EXISTS("ADMIN_USER not exists", "621","ADMIN_USER不存在"),
	ADMIN_USER_IS_DISABLED("ADMIN_USER is disabled", "622","ADMIN_USER已停用"),
	ADMIN_USER_PASSWORD_NOT_MATCH("ADMIN_USER password not match", "623","帳號或密碼錯誤"),
	GET_ADMIN_USERS_ERRPR("get ADMIN_USERs error", "624","取單筆ADMIN_USER發生錯誤"),
	GET_ADMIN_USER_ERRPR("get ADMIN_USER error", "625","取多筆ADMIN_USER發生錯誤"),
	ADD_ADMIN_USER_ERRPR("add ADMIN_USER error", "626","新增ADMIN_USER發生錯誤"),
	UPDATE_ADMIN_USER_PASSWORD_ERRPR("update ADMIN_USER password error", "627","更新ADMIN_USER密碼發生錯誤"),
	CHECK_ADMIN_USER_PASSWORD_ERRPR("check ADMIN_USER password error", "628","檢查ADMIN_USER密碼發生錯誤"),
	ADMIN_USER_STATUS_NOT_MATCH("ADMIN_USER status not match", "629","錯誤的ADMIN_USER狀態"),
	UPDATE_ADMIN_USER_ERROR("update ADMIN_USER error", "630","更新ADMIN_USER發生錯誤"),
	ADMIN_USER_FAIL_TO_LOGIN("ADMIN_USER fail to login", "631","ADMIN_USER登入失敗"),
	ADMIN_USER_NOT_MATCH("ADMIN_USER can not match with ADMIN_USER id", "632","ADMIN_USER編號無法match"),

	/** role 相關*/
	ROLE_ID_IS_NOT_CORRECTED("role id is not corrected", "706","角色ID錯誤"),
	ROLE_NAME_IS_EXISTED("role name is existed", "707","角色名稱重覆"),
	DELETE_ROLE_IN_USE("role in use can not delete", "708", "無法刪除使用中的角色"),
	ROLE_FUNCTION_ERROR("role functions setting error","720", "角色權限設定錯誤");

	private String message;
	private String code;
	private String chtMessage;

	ResponseStatus(String message, String code, String chtMessage) {
		this.message = message;
		this.code = code;
		this.chtMessage = chtMessage;
	}
	public String getMessage() {
		return message;
	}
	public String getCode() {
		return code;
	}
	public String getChtMessage() {
		return chtMessage;
	}

	public static void toHtml() throws IOException {
		FileWriter fw = new FileWriter("src/main/resources/static/errorCodeList.html");
		BufferedWriter writer = new BufferedWriter(fw);

		ResponseStatus[] responses = ResponseStatus.values();
		StringBuilder  sb = new StringBuilder();
		sb.append("<!DOCTYPE html>\r\n" +
				"<html>\r\n" +
				"<head>\r\n" +
				"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n" +
				"<style>\r\n" +
				"body {\r\n" +
				"  text-align: center;\r\n" +
				"}\r\n" +
				"table {\r\n" +
				"  border-collapse: collapse;\r\n" +
				"  border-spacing: 0;\r\n" +
				"  border: 1px solid #eee;\r\n" +
				"  color: #333;\r\n" +
				"  width: 800px;\r\n" +
				"  margin: 0 auto;\r\n" +
				"}\r\n" +
				"th, td {\r\n" +
				"  text-align: left;\r\n" +
				"  padding: 0.8em 1.2em;\r\n" +
				"  margin: 0;\r\n" +
				"  overflow: visible;\r\n" +
				"  background: #fff;\r\n" +
				"}\r\n" +
				"th {\r\n" +
				"  background-color: #333;\r\n" +
				"  color: #fff;\r\n" +
				"  text-align: left;\r\n" +
				"  vertical-align: bottom;\r\n" +
				"}\r\n" +
				"td:first-child, th:first-child {\r\n" +
				"  border-left-width: 0;\r\n" +
				"}\r\n" +
				"tr:nth-child(2n-1) td {\r\n" +
				"  background-color: #f2f2f2;\r\n" +
				"}\r\n" +
				"td.lable {\r\n" +
				"  padding: 0.5em 1em;\r\n" +
				"  text-align: center;\r\n" +
				"  background-color: #909090 !important;\r\n" +
				"  color: #fff;\r\n" +
				"}\r\n" +
				"</style>\r\n" +
				"</head>\r\n" +
				"<body>\r\n" +
				"\r\n" +
				"<h2>Error Code List</h2>\r\n" +
				"\r\n" +
				"<table>\r\n" +
				"  <tr>\r\n" +
				"    <th width=\"50\">錯誤碼</th>\r\n" +
				"    <th>錯誤描述(英文)</th>\r\n" +
				"    <th width=\"220\">錯誤描述</th>\r\n" +
				"  </tr>");
		for (ResponseStatus responseStatus : responses) {
			sb.append("<tr>");
			sb.append("  <td>" + responseStatus.getCode() + "</td>");
			sb.append("  <td>" + responseStatus.getMessage() + "</td>");
			sb.append( " <td>" + responseStatus.chtMessage + "</td>");
			sb.append("</tr>");
		}
		sb.append("</table>\r\n" +
				"\r\n" +
				"</body>\r\n" +
				"</html>");
		writer.write(sb.toString());
		writer.flush();
		writer.close();
		fw.close();
	}
}

