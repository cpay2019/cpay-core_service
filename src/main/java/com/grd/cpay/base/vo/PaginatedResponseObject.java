package com.grd.cpay.base.vo;

import io.swagger.annotations.ApiModelProperty;

public class PaginatedResponseObject<T> extends ResponseObject<T> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value="當前頁數")
    private int page;
    @ApiModelProperty(value="總頁數")
    private int totalPage;
    @ApiModelProperty(value="總筆數")
    private int totalRecords;

    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }
    public int getTotalPage() {
        return totalPage;
    }
    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
    public int getTotalRecords() {
        return totalRecords;
    }
    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

}
