package com.grd.cpay.base.vo;

public class ResponseObject<T> {
	private String message;
	private String code;
	private T data;
	
	
	public ResponseObject() {
		this.message = ResponseStatus.SUCCESS.getMessage();
		this.code=ResponseStatus.SUCCESS.getCode();
	}
	
	public ResponseObject(T t) {
		this.message = ResponseStatus.SUCCESS.getMessage();
		this.code=ResponseStatus.SUCCESS.getCode();
		this.data = t;
	}

	public ResponseObject(String message) {
		this.code=ResponseStatus.FAIL.getCode();
		this.message = message;
	}
	
	public ResponseObject(ResponseStatus s) {
		this.code = s.getCode();
		this.message = s.getMessage();
	}
	
	public ResponseObject(ResponseStatus s, String message) {
		this.code = s.getCode();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
