package com.grd.cpay.base.vo;

import io.swagger.annotations.ApiModelProperty;

public class PaginatedRequestObject {

    @ApiModelProperty(value="顯示筆數，default為20", required=false, example = "20")
    private int pageSize = 20;
    @ApiModelProperty(value="查詢頁數，default為1", required=false, example = "1")
    private int page = 1;

    public int getPageSize() {
        return pageSize;
    }
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }
}
