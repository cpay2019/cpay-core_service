package com.grd.cpay.base.enums;

public enum ChannelType {

    ALI("支付寶"),
    WECHAT("微信"),
    UNION("雲閃付");

    private String display;

    ChannelType(String display) {
        this.display = display;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }
}
