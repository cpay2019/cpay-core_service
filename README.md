# Combo Pay

## 1. Introduction
- Combo Pay是一個聚合支付，整合支付寶、雲閃付...等相關第三支付。  
- 提供快速便捷的支付服務，快速提供商家進行支付串接與多帳戶整合。

## 2. Get started
### 2.1 Datebase
- **Create local Database**
```
docker run --name cpay -p 3306:3306 -e MYSQL_ROOT_PASSWORD=root -d mysql:5.7 mysqld
```
- **Table Schema & Init Data**  
[V2018121301__Base_version.sql](https://gitlab.com/cpay2019/cpay-core_service/blob/master/src/test/resources/db/V2018121301__Base_version.sql)

- **Modify Database Information**  
[applicattion.yml](https://gitlab.com/cpay2019/cpay-core_service/blob/master/src/main/resources/application.yml)
```
spring.datasource:
  url: jdbc:mysql://localhost:3306/cpay?useSSL=false&characterEncoding=utf8
  username: root
  password: root
  maxActive: 500
```

### 2.2 Payment Gateway Service
- **Source Code**  
https://gitlab.com/cpay2019/cpay-payment_gateway 

- **Docker Image**
```
docker push registry.gitlab.com/cpay2019/cpay-payment_gateway
```

### 2.3 Run
- **Exec** [CpayCoreApplication.java](https://gitlab.com/cpay2019/cpay-core_service/blob/master/src/main/java/com/grd/cpay/CpayCoreApplication.java)
- **Demo Page**  http://localhost:8080/payment/demo
- **Console Page** http://localhost:8080/

## 3. Demo
### 3.1 Payment Page
![](https://gitlab.com/cpay2019/cpay-core_service/raw/master/src/main/resources/static/images/payment-01.jpg)
![](https://gitlab.com/cpay2019/cpay-core_service/raw/master/src/main/resources/static/images/payment-02.jpg)
![](https://gitlab.com/cpay2019/cpay-core_service/raw/master/src/main/resources/static/images/payment-03.jpg)
![](https://gitlab.com/cpay2019/cpay-core_service/raw/master/src/main/resources/static/images/payment-04.jpg)

### 3.2 Admin Console
![](https://gitlab.com/cpay2019/cpay-core_service/raw/master/src/main/resources/static/images/console-01.jpg)
![](https://gitlab.com/cpay2019/cpay-core_service/raw/master/src/main/resources/static/images/console-02.jpg)
![](https://gitlab.com/cpay2019/cpay-core_service/raw/master/src/main/resources/static/images/console-03.jpg)